all: stamp

stamp:
	ruby retrieve.rb
	touch stamp

clean:
	$(RM) stamp

reload:
	$(MAKE) clean
	$(MAKE)
