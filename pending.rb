# XXX This script must be in a Debian sid box, with apt and apt-file databases
# up to date.

# If your sources.list contains more than unstable, please supply a local
# sources.list file only containing an entry for unstable.

team_packages_cache = '/tmp/pkg-ruby-extras.packages'
if File.exists?(team_packages_cache)
  $team_packages = open(team_packages_cache).read.split
else
  require 'open-uri'
  $team_packages = open('http://pkg-ruby-extras.alioth.debian.org/cgi-bin/packages').read.split
  File.open(team_packages_cache, 'w') do |f|
    f.puts($team_packages)
  end
end

$pending = {}
$whitelist = %w[
  ruby-defaults
  ruby1.8
  ruby1.9.1
  ohai
  amrita2
  ruby-setup
]

def search(test, shell_command)
  binary_packages = `#{shell_command}`.split
  binary_packages.map do |pkg|
    `apt-cache show #{pkg} | grep 'Source:' | cut -d " " -f 2`.split.first || pkg
  end.reject do |pkg|
    $whitelist.include?(pkg)
  end.each do |pkg|
    team = $team_packages.include?(pkg)
    $pending[team] ||= {}
    $pending[team][pkg] ||= []
    $pending[team][pkg] << test
  end
end

at_exit do
  [true, false].each do |team|
    packages = $pending[team]
    puts "# Packages #{ team && '' || 'NOT '}maintained by the team (#{packages.keys.size})"
    packages.keys.sort.each do |key|
      puts "#{key}: #{$pending[team][key].uniq.join(', ')}"
    end
  end
end

search :wrong_name, 'aptitude search "~n^lib.*-ruby !~D^ruby- ~Aunstable" -F %p'

if FileTest.readable? "sources.list"
  search :wrong_install_location, 'apt-file -s sources.list -l search /usr/lib/ruby/1'
else
  search :wrong_install_location, 'apt-file -l search /usr/lib/ruby/1'
end
