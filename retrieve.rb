require 'open-uri'
require 'json'
require 'fileutils'
require 'pg'
require 'date'
require 'erb'

data = []

# list of source packages that don't need to be migrated
DONT_MIGRATE_SRC = [
  'ruby-defaults',
  'ruby1.8',
  'ruby1.9.1',
  'gem2deb',
  'ruby-gnome2', # cannot use gem2deb
  'ruby-pkg-tools', # old packaging helper
  'rails', # already migrated, contains only transitional packages
]

# accumulate data series points
if File.exists?('data.json')
  FileUtils.cp('data.json', 'data.json~')
  data = JSON.load(File.read('data.json'))
end

# get new data
connection = PG.connect({:host => 'udd.debian.org', :port => 5452, :user => 'guest', :dbname => 'udd'})

pending_packages_query = <<EOF
select distinct s.source as source, p.package as binary, s.architecture as arch, coalesce(pc.insts, 0) as popcon, coalesce(pcs.insts, 0) as popcon_src, s.maintainer as maintainer, s.uploaders as uploaders
from sources_uniq s, packages p
left join popcon pc on (p.package = pc.package)
left join popcon_src pcs on (p.source = pcs.source)
where s.distribution = 'debian' and s.release='sid'
and p.distribution = 'debian' and p.release='sid' and s.source = p.source
and (s.source ~ 'lib.*-ruby' or p.package ~ 'lib.*-ruby.*' or s.build_depends ~ '.*ruby-pkg-tools.*' or s.build_depends_indep ~ '.*ruby-pkg-tools.*' or p.depends ~ '.*ruby1.8.*')
order by source, p.package;
EOF

done_packages_query = <<EOF
select distinct s.source as source, p.package as binary, s.architecture as arch, coalesce(pc.insts, 0) as popcon, s.maintainer as maintainer, s.uploaders as uploaders, p.description as descr
from sources_uniq s, packages p
left join popcon pc on (p.package = pc.package)
where s.distribution = 'debian' and s.release='sid'
and p.distribution = 'debian' and p.release='sid' and s.source = p.source
and (s.build_depends ~ '.*gem2deb.*')
order by source, p.package;
EOF

new_packages_query = <<EOF
select source, package, maintainer from new_packages
where package ~ 'ruby' or source ~ 'ruby' or maintainer ~ 'ruby';
EOF

pending_packages = connection.exec(pending_packages_query)
done_packages = connection.exec(done_packages_query)
new_packages = connection.exec(new_packages_query)

done_source_packages = done_packages.map { |pkg| pkg['source'] }.uniq
pending_packages = pending_packages.to_a
done_binary_packages = done_packages.map { |pkg| pkg['binary'] }
pending_packages.reject! { |pkg| done_binary_packages.include?(pkg['binary']) }
pending_packages.reject! { |pkg| DONT_MIGRATE_SRC.include?(pkg['source']) }
pending_source_packages = pending_packages.map { |pkg| pkg['source'] }.uniq

pending_packages_gr = pending_packages.group_by { |pkg| { 'source' => pkg['source'], 'arch' => pkg['arch'], 'popcon_src' => pkg['popcon_src'], 'maintainer' => pkg['maintainer'], 'uploaders' => pkg['uploaders'] } }
pending_packages_gr.each_pair do |k, v|
  pending_packages_gr[k] = v.map { |pkg| "#{pkg['binary']} (#{pkg['popcon']})" }.join(', ')
end
pending_packages_gr = pending_packages_gr.to_a.map do |h, v|
  h['binaries'] = v
  h
end
pending_packages_gr.sort! { |a,b| a['popcon_src'].to_i <=> b['popcon_src'].to_i }
pending_packages_gr.reverse!
pending_packages_gr_team = pending_packages_gr.select { |pkg| pkg['maintainer'] =~ /pkg-ruby-extras/ or pkg['uploaders'] =~ /pkg-ruby-extras/ }
pending_packages_gr_noteam = pending_packages_gr.select { |pkg| not (pkg['maintainer'] =~ /pkg-ruby-extras/ or pkg['uploaders'] =~ /pkg-ruby-extras/) }

new_time_data = {
  'date' => Date.today.strftime('%Y%m%d').to_i,
  'pending' => pending_packages.count,
  'done' => done_packages.count,
}
data << new_time_data

# filter out transitional done packages
done_packages = done_packages.reject { |pkg| pkg['descr'] =~ /Transitional package/ }

# write data files
File.open('data.json', 'w') do |f|
  f.write(data.to_json)
end
File.open('details.html', 'w') do |f|
  template = ERB.new(File.read('details.html.erb'))
  f.puts(template.result(binding))
end
