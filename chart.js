/* Plan is to freeze Wheezy on June 2012. We must be done by the end of May
 * 2012.
 *
 * Ref: http://lists.debian.org/debian-devel-announce/2011/06/msg00003.html
 */
const WHEEZY_FREEZE_DATE = new Date(2012, 5, 15);

const ONE_MONTH = 30*1000*60*60*24;

function to_date_string(date) {
  return date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate();
}

function to_proper_date(date_as_number) {
  var year = Math.floor(date_as_number / 10000);
  var month = Math.floor(date_as_number % 10000 / 100);
  var day = Math.floor(date_as_number % 100);
  return new Date(year, month - 1, day);
}

$(function() {
  $.getJSON('data.json', function(input) {

    var pending = [];
    var done = [];
    var initial_date = null;
    var final_date = null;
    var maximum_pending = 0;

    $.each(input, function(index, point) {
      var date = to_proper_date(point.date);
      pending.push([date.getTime(), point.pending]);
      done.push([date.getTime(), point.done]);
      if (initial_date == null || date < initial_date) { initial_date = date; }
      if (final_date == null || date > final_date) { final_date = date; }
      if (point.pending > maximum_pending) {
        maximum_pending = point.pending;
      }
    });

    var ideal_line = {
      label: "Ideal progress",
      data: [[initial_date, maximum_pending],[WHEEZY_FREEZE_DATE, 0]],
      color: "#eeeeec",
      points : { show: false },
      lines: { show: true}
    }

    $.plot(
      $('#progress'),
      [
        ideal_line,
        { label: "Pending", data: pending, color: "#a40000" },
        { label: "Done", data: done, color: "#4e9a06" }
      ],
      {
        series: { points: { show: false }, lines: { show: true } },
        grid: { hoverable: true },
        xaxis: {
          min: initial_date.getTime() - ONE_MONTH,
          max: WHEEZY_FREEZE_DATE.getTime() + ONE_MONTH, // leave some empty space in the chart
          ticks: [
            [initial_date, to_date_string(initial_date) + '<br/>(tracking starts)'],
            [final_date, to_date_string(final_date) + '<br/>(today)'],
            [WHEEZY_FREEZE_DATE, to_date_string(WHEEZY_FREEZE_DATE) + '<br/>(Wheezy freeze)']
          ]
        }
      }
    );

    // based on http://people.iola.dk/olau/flot/examples/interacting.html
    function showTooltip(x, y, contents) {
      $('<div id="tooltip">' + contents + '</div>').css( {
        position: 'absolute',
        display: 'none',
        top: y - 30,
        left: x + 10,
        border: '1px solid #c4a000',
        padding: '2px',
        'background-color': '#fce94f',
        opacity: 0.80
      }).appendTo("body").fadeIn(200);
    }

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var previousPoint = null;
    $("#progress").bind("plothover", function (event, pos, item) {
      if (item) {
        if (previousPoint != item.dataIndex) {
          previousPoint = item.dataIndex;

          $("#tooltip").remove();
          var date = new Date(item.datapoint[0]);
          var tooltip = months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear() + ': <strong>' + item.datapoint[1] + '</strong>';
          showTooltip(item.pageX, item.pageY, tooltip)
        }
      }
      else {
        $("#tooltip").remove();
        previousPoint = null;
      }
    });


  });
});
